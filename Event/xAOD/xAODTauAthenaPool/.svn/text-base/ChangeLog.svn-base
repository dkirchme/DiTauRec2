2015-03-11 Will Buttinger <will@cern.ch>
	* src/xAODTauJetAuxContainerCnv_v1.cxx: Changed output level to debug
	* tagging as xAODTauAthenaPool-00-01-06
	
2015-01-12 Michel Janus <janus@cern.ch>
	* tagging as xAODTauAthenaPool-00-01-05
	* bugfix for nPi0Topo variable in converter and taking out warning message
	* tagging as xAODTauAthenaPool-00-01-04
	* fixing v1 to v2 converter

2014-12-13  scott snyder  <snyder@bnl.gov>

	* Tagging xAODTauAthenaPool-00-01-03.
	* src/xAODTauAthenaPoolTPCnv.cxx: Use copyThinned to copy the
	aux stores when writing.

2014-12-01 Michel Janus <janus@cern.ch>
	* tagging as xAODTauAthenaPool-00-01-02
	* finishing off v1->v2 converter with variables with standalone setter/getter
	* adding details and pantau details to v1->v2 converter
	* adding element links to v1->v2 converter

2014-11-29 Michel Janus <janus@cern.ch>
	* adding all 4-vectors and PID decisions to v1->v2 converter

2014-11-29 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Removed the toPersistent() calls from the code, as these
	  are no longer necessary. (We make sure that ElementLinks
	  are written out correctly, by using a custom ROOT streamer
	  for ElementLinkBase.)
	* Implemented the infrastructure for schema evolution support
	  in the package.
	* The converter for xAOD::TauJetContainer is fully functional
	  as is.
	* The converter for xAOD::TauJetAuxContainer is working, but it's
	  not doing "the full job" yet. It will still need to receive
	  a fair amount of code for copying all the payload from the
	  _v1 object to the _v2 one. For now just some 4-momentum
	  variables are getting transferred. (Just to check that the
	  code works in principle.)
	* Tagging as xAODTauAthenaPool-00-01-01

2014-11-28 Michel Janus <janus@cern.ch>
	* tagging as xAODTauAthenaPool-00-01-00
	* adapt to removal of track links and renaming of pfo links
	
2014-11-28 Blake Burghgrave <blake.burghgrave AT cern.ch>
	* adapt to changes in TauJet_v2

2014-11-28 Michel Janus <janus@cern.ch>
	* removing eflowrec pfos and adding hadronic pfos to converter
	
2014-02-05 Michel Janus <janus@cern.ch>
	* adapting to change in element link vector accessor renaming
	* Tagging as xAODTauAthenaPool-00-00-04
	
2014-03-27 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Calling toPersistent() on all the ElementLinks in
	  xAODTauJetContainerCnv.
	* Since all the ElementLinks are static variables at this
	  point, the code is very simple. But they should be made
	  dynamic later on, at which point the code should become
	  a bit more careful.
	* Because of problems in tau reconstruction, this new tag
	  actually breaks the file writing. (Vertex link being set
	  up in an invalid state.) Needs to be fixed before starting
	  to use this tag.
	* Tagging as xAODTauAthenaPool-00-00-03

2014-02-05 Michel Janus <janus@cern.ch>
	* adding explicit converters for TauJet- and TauJetAuxContainer
	* Tagging as xAODTauAthenaPool-00-00-02

2013-12-10 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Setting the metadata on the package.
	* Creating a first tag for MIG14.
	* Tagging as xAODTauAthenaPool-00-00-01

2013-11-09 Michel Janus <janus@cern.ch>
	* Created a set of default POOL converters for the tau
	  classes. 
